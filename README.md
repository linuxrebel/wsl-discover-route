# WSL-Discover-Route
## I've learned that in order to get your actual LAN network you need to do a bit of trickery and create an External Virtual Switch called "Primary Virtual Switch" (this is the name I used and coded for you can call it Ethel if you'd like) Once this exists your WSL install will automaticallly start to use this switch and you will be properly presented with the necessary IPs for proper routing.  (Instructions )
https://docs.microsoft.com/en-us/virtualization/hyper-v-on-windows/quick-start/connect-to-network.  


**append this to your .bashrc to get the network you're on for your laptop.**
**You will be able to launch multiple sessions without issue as this script is idempotent**
**Remember there is a reason for this being in bash.  It's going into a bashrc script**

**Variables created **
* parent_HOST = Queries Windows to get the IP windows has                                  
* dat1 dat2 dat3 dat4 dat5 dat6 dat7 dat8 = Series of data points it grabs IF your system already has a route (for example you moved location/network)
* rip1 rip2 rip3 rip4 = takes apart your IP of an existing route
* sec_short_ip = Creates the existing route's short (minus last octet) IP    
* gateway_HOST = This is the gateway for routing to the hosts network (Literally the same as your host uses to get to that route)
* mask_HOST = Right now this is assumed to be a /24 
* eth_device = Usually eth0, but just in case of systemD ... we discover
* cip1 cip2 cip3 cip4 = take apart your host ip so we can determine your network
* short_ip = short IP (minus the last octet) or your new host IP network        

**You will possibly need to adjust 3rd line where it says wireless If you have created a new External Switch in HyperV, or if for some reason your laptop is different in naming your wireless interface than my 2 test laptops where.  Run ipconfig in power shell and make sure you have the keyword "wireless" in the name of the WiFi device you which to do discovery on.** 

**Once all this is done the script will do the following**

* Determine if a route currently exists
  * if it doesn't add the route
* else if it does exist
  * Determine if what is desired is the same as what exists
  * if they are the same, do nothing
  * if they are different
    * Remove the old route
    * Add the new route

**This is all done in a fuction and called into the background so that it doesn't slow down the launch of the wsl session.**
